
## Cara Instalasi
Cloning  repo:
```shell
git clone https://gitlab.com/trismanhady/perpus.git
```

Install composer packages:
```shell
composer update
```

Copy dan rename .env.example to .env.
Buat database di PHPmyAdmin dengan nama db_perpus

Masuk ke shell ketikkan
```shell
php artisan key:generate
```

Lakukan migrate dan update isi database user:
```shell
php artisan migrate
```
```shell
php artisan db:seed
```

ketikkan di browser <div style="display: inline">http://localhost:8000/</div>

#### Admin Login
*  Username: admin123
*  Password: admin123

#### Anggota Login
*  Username: user123
*  Password: user123


